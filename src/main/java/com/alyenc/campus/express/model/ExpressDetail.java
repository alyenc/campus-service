package com.alyenc.campus.express.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ExpressDetail {

    private String context;

    private String ftime;

    private String fdate;
}

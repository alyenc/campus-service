package com.alyenc.campus.express.enums;

public enum StateEnum {

    ON_THE_WAY(0, "在途中"),
    RECIEVED(1, "已揽收"),
    PROBLEM(2, "疑难");


    private int code;

    private String desc;

    // 构造方法
    StateEnum(int code, String desc) {
        this.desc = desc;
        this.code = code;
    }

    public static String getDescByCode(String code) {
        for (StateEnum state : StateEnum.values()) {
            if (code.equals(state.code)) {
                return state.desc;
            }
        }
        return null;
    }
}

package com.alyenc.campus.express.enums;

public enum ExpressEnum {

    YUN_DA("yunda","韵达快递");


    private String code;

    private String name;

    /**
     * 构造方法
     * @param code
     * @param name
     * @return
     */
    ExpressEnum(String code, String name) {
        this.name = name;
        this.code = code;
    }

    public static String getNameByCode(String code) {
        for (ExpressEnum expr : ExpressEnum.values()) {
            if (code.equals(expr.code)) {
                return expr.name;
            }
        }
        return null;
    }
}

package com.alyenc.campus.express;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alyenc.campus.express.enums.ExpressEnum;
import com.alyenc.campus.express.enums.StateEnum;
import com.alyenc.campus.express.model.ExpressDetail;
import com.alyenc.campus.express.response.ExpressCodeResponse;
import com.alyenc.campus.express.response.ExpressResponse;
import com.alyenc.campus.utils.ExpressCode;
import com.alyenc.campus.utils.HttpUtil;
import com.alyenc.campus.utils.MD5Util;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExpressServiceImpl implements ExpressService {

    @Override
    public ExpressResponse getExpress(String com, String num) {
        String param = String.format("{\"com\":\"%s\",\"num\":\"%s\",\"from\":\"\",\"to\":\"\",\"resultv2\":0}", com, num);
        String customer ="67EC5BFE39E38A5D0EFC27824A6A641E";
        String key = "jwgwGaJn3400";
        String sign = MD5Util.encode(param + key + customer, "UTF-8").toUpperCase();
        Map<String, Object> params = new HashMap<>();
        params.put("param",param);
        params.put("sign",sign);
        params.put("customer",customer);
        String resp;
        try {
            System.out.println(params);
            resp = HttpUtil.sendGet("http://poll.kuaidi100.com/poll/query.do", null, params);
            JSONObject respJSON = JSON.parseObject(resp);
            String msg = respJSON.getString("message");

            ExpressResponse response = new ExpressResponse();
            List<ExpressDetail> exprPath = new ArrayList<>();

            System.out.println(respJSON);
            if(("ok").equals(msg)){
                String respCom = respJSON.getString("com");
                String respState = respJSON.getString("state");
                String respCode = respJSON.getString("nu");

                response.setExprCompany(Optional.ofNullable(respState).map(item -> ExpressEnum.getNameByCode(respCom)).orElse(null));
                response.setState(Optional.ofNullable(respState).map(item -> StateEnum.getDescByCode(respState)).orElse(null));
                response.setCode(Optional.ofNullable(respCode).orElse(null));

                JSONArray respArray = respJSON.getJSONArray("data");
                for(int i = 0; i < respArray.size(); i++) {
                    ExpressDetail detail = new ExpressDetail();
                    JSONObject dataObject = respArray.getJSONObject(i);
                    String respContext = dataObject.getString("context");
                    String time = dataObject.getString("time");

                    String[] timearray = time.split(" ");
                    detail.setContext(Optional.ofNullable(respContext).orElse(null));
                    detail.setFdate(Optional.ofNullable(timearray[0]).orElse(null));
                    detail.setFtime(Optional.ofNullable(timearray[1]).orElse(null));
                    exprPath.add(detail);
                }
                response.setExprPath(exprPath);
            }
            System.out.println(response);

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ExpressCodeResponse getExpressCode() {
        List<String> code = ExpressCode.code;
        ExpressCodeResponse response = new ExpressCodeResponse();

        List<Map<String, String>> codeList = code.stream().map(item -> {
            Map<String, String> map = new HashMap<>();
            String[] splitStr = item.split(",");
            map.put("code", splitStr[0]);
            map.put("com", splitStr[1]);
            return map;
        }).collect(Collectors.toList());
        response.setExpressCode(codeList);
        return response;
    }
}

package com.alyenc.campus.express.response;

import com.alyenc.campus.express.model.ExpressDetail;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ExpressResponse {

    /**
     * 快递公司
     */
    private String exprCompany;

    /**
     * 快件状态
     */
    private String state;

    /**
     * 快递单号
     */
    private String code;

    /**
     * 快件路径
     */
    private List<ExpressDetail> exprPath;
}

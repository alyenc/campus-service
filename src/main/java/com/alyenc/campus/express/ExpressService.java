package com.alyenc.campus.express;

import com.alyenc.campus.express.response.ExpressCodeResponse;
import com.alyenc.campus.express.response.ExpressResponse;

public interface ExpressService {

    ExpressResponse getExpress(String com, String num);

    ExpressCodeResponse getExpressCode();
}

package com.alyenc.campus.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class DealLinkUtil {

    private static final String prefix="https://campus.alyenc.com/qlu";

    public static String modifyLink(Document doc) throws MalformedURLException, IOException {
        Elements elements=doc.select("a[href!=#]");
        adsoluteAHref(elements);
        Elements jsElements=doc.select("script[src]");
        absoluteScriptSrc(jsElements);

        Elements formElements=doc.select("form[action]");
        absoluteFormAction(formElements);

        Elements linkElements=doc.select("link[href]");
        absoluteLinkHref(linkElements);

        Element  base=doc.select("base").first();
        if(base!=null){
            System.out.println(base.attr("href"));
            base.attr("href", prefix);}else{
            Element head=doc.select("head").first();
            head.append("<base href=\""+prefix+"\">");
        }

        return doc.toString();
    }

    //处理<script type="text/javascript">脚本
    public static void IteratorStyle(Elements elements){
        for (Element element : elements) {
            System.out.println(element.data());//获得<script type="text/javascript">中的值
            //<style type="text/css"></style>中的值
        }
    }

    //得到元素中内嵌样式style中的值如<div style="width:100px;">
    public static void IteratorElements(Elements elements){
        for (Element element : elements) {
            String style = element.attr("style");
            System.out.println(style);
            getURL(style);
        }
    }
    //从style中获得 url的值
    private static void getURL(String url){
        Pattern p = Pattern.compile("url\\((.*)\\)");//匹配  url(任何)
        Matcher m = p.matcher(url);
        if(m.find()){
            System.out.println(m.group(1));//获取括号中的地址
        }
    }
    //将Form action 装换为绝对的url
    private static void absoluteFormAction(Elements formElements){
        for (Element element : formElements) {
            String action = element.attr("abs:action");//将所有的相对地址换为绝对地址;
            //添加隐藏域，用来传替url。
            element.append("<input type='hidden' name='action' value='" + action + "'/>");
            element.attr("action", prefix + "Actionjsp");//装换为

        }
    }

    //将<script src>转换为绝对地址
    private static void absoluteScriptSrc(Elements jsElements) throws MalformedURLException{
        for (Element element : jsElements) {
            String src = element.attr("abs:src");//将所有的相对地址换为绝对地址;
            element.attr("src", prefix + "Jsjsp?src=" + src);//装换为
        }
    }

    //将Img src 装换为绝对的url
    public static void absoluteImagSrc(Elements imagElements) throws MalformedURLException{
        for (Element element : imagElements) {
            String src = element.attr("abs:src");//将所有的相对地址换为绝对地址;
            element.attr("src", prefix + "Imgjsp?src=" + src);//装换为

        }
    }

    //将Link href 装换为绝对的url
    private static void absoluteLinkHref(Elements linkElements) throws MalformedURLException{
        for (Element element : linkElements) {
            String src = element.attr("abs:href");//将所有的相对地址换为绝对地址;
            element.attr("href", prefix + "Linkjsp?href=" + src);//装换为
        }
    }
    //将所有的的<a href>转换为绝对地址
    private static void adsoluteAHref(Elements AElements){
        for (Element element : AElements) {
            String href = element.attr("abs:href");//将所有的相对地址换为绝对地址;
            element.attr("href", prefix + "Ajsp?url=" + href);
        }
    }


    public static void main(String args[]) throws ParseException, IOException{
	/*	HttpClient httpClient=TestCookie.GetHttpClient();
		String htmlpage=TestCookie.getPageByGet(httpClient, "http://book.hao123.com/");
		System.out.println(modifyLink(htmlpage,"http://localhost:8080/Proxy"));*/
    }
}

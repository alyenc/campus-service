package com.alyenc.campus.news;

import com.alyenc.campus.news.model.NewsListResponse;

import java.util.List;
import java.util.Map;

public interface NewsService {

    NewsListResponse getNewsList();

    List<Map<String, Object>> getNewsDetail(String url);
}

package com.alyenc.campus.news.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
public class NewsListResponse {

    private List<NewsDetail> newsList;
}

package com.alyenc.campus.news.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class NewsDetail {

    private String title;
    private String desc;
    private String publishTime;
    private String from;
    private String editor;
    private String author;
    private String url;
}

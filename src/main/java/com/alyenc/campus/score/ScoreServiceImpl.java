package com.alyenc.campus.score;

import com.alyenc.campus.score.model.*;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class ScoreServiceImpl implements ScoreService {

    private final static String URLSAFECODE = "http://jwxt.qlu.edu.cn/verifycode.servlet?t=0.020974584"; // 验证码

    private final static String URLENCODE = "http://jwxt.qlu.edu.cn/Logon.do?method=logon&flag=sess"; // 加密字符串

    private final static String URLLOGIN = "http://jwxt.qlu.edu.cn/Logon.do?method=logon"; // 登录

    private final static String URLSCORE = "http://jwxt.qlu.edu.cn/jsxsd/kscj/cjcx_list";

    private final static String URLCOURSE = "http://jwxt.qlu.edu.cn/jsxsd/xskb/xskb_list.do";

    private Map<String, String> cookie;


    @Override
    public VerifyCodeResponse getVerifyCode() {
        VerifyCodeResponse response = new VerifyCodeResponse();
        try {
            String code = getSafeCode();
            response.setCookie(cookie);
            response.setVerifyCode(code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public ScoreResponse getScore(String num, String pass, String code, String cookie) {
        ScoreResponse response = new ScoreResponse();
        try {
            initLogin(num, pass, code, cookie);
            List<ScoreDetail> details = getScore(cookie);
            response.setScores(details);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public CourseResponse getCourses(String num, String pass, String code, String cookie, String condition) {
        CourseResponse response = new CourseResponse();
        try {
            initLogin(num, pass, code, cookie);
            List<CourseDetail> details = getCourses(cookie, condition);
            response.setCourses(details);
        }catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    public CourseResponse reloadCourses(String cookie, String condition) {
        CourseResponse response = new CourseResponse();
        try {
            List<CourseDetail> details = getCourses(cookie, condition);
            response.setCourses(details);
        }catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private List<CourseDetail> getCourses(String cookie, String condition) {
        List<CourseDetail> details = new ArrayList<>();
        try {
            Map<String, String> data = new HashMap<>();
            data.put("xnxq01id", condition);
            data.put("sfFD", "1");
            Connection connect = Jsoup.connect(URLCOURSE)
                    .header("Accept",
                            "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                    .userAgent("Mozilla").method(Connection.Method.POST).data(data).timeout(3000);
            connect.cookie("JSESSIONID", cookie);
            Connection.Response response = connect.execute();
            Document document = response.parse();
            Element element = document.getElementById("kbtable");
            Elements trs = element.select("tr");
            for (Element tr : trs) {
                Elements tds = tr.select("td");
                if (!tds.isEmpty() && tds.size() >= 7) {
                    Element th = tr.select("th").first();
                    CourseDetail courseDetail = CourseDetail.builder()
                            .title(th.text())
                            .monday(tds.get(0).select("div").last().text())
                            .thursday(tds.get(1).select("div").last().text())
                            .wednesday(tds.get(2).select("div").last().text())
                            .thursday(tds.get(3).select("div").last().text())
                            .friday(tds.get(4).select("div").last().text())
                            .saturday(tds.get(5).select("div").last().text())
                            .sunday(tds.get(6).select("div").last().text()).build();

                    details.add(courseDetail);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return details;
    }

    private List<ScoreDetail> getScore(String cookie) {
        List<ScoreDetail> details = new ArrayList<>();
        try {
            Map<String, String> data = new HashMap<>();
            data.put("xsfs", "all");
            Connection connect = Jsoup.connect(URLSCORE)
                    .header("Accept",
                            "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                    .userAgent("Mozilla").method(Connection.Method.POST).data(data).timeout(3000);
            connect.cookie("JSESSIONID", cookie);
            Connection.Response response = connect.execute();
            Document document = response.parse();
            Element element = document.getElementById("dataList");
            Elements trs = element.select("tr");
            for (Element tr : trs) {
                Elements tds = tr.select("td");
                if (!tds.isEmpty()) {
                    ScoreDetail scoreDetail = ScoreDetail.builder()
                            .term(tds.get(1).text())
                            .name(tds.get(3).text())
                            .number(tds.get(2).text())
                            .score(tds.get(4).text())
                            .credit(tds.get(5).text())
                            .point(tds.get(7).text())
                            .attr(tds.get(9).text()).build();
                    details.add(scoreDetail);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return details;
    }

    private void initLogin(String num, String pass, String code, String cookie) throws IOException {
        try {
            Map<String, String> data = new HashMap<>();
            data.put("view", "1");
            data.put("encoded", getEncoded(num, pass));
            data.put("RANDOMCODE", code);
            Connection connect = Jsoup.connect(URLLOGIN)
                    .header("Accept",
                            "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                    .userAgent("Mozilla").method(Connection.Method.POST).data(data).timeout(3000);
            connect.cookie("JSESSIONID", cookie);
            Connection.Response response = connect.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getSafeCode() throws IOException {
        Connection.Response response = Jsoup.connect(URLSAFECODE).ignoreContentType(true) // 获取图片需设置忽略内容类型
                .userAgent("Mozilla").method(Connection.Method.GET).timeout(3000).execute();
        cookie = response.cookies();
        byte[] bytes = response.bodyAsBytes();
        Base64.Encoder base64Encoder = Base64.getEncoder();
        return base64Encoder.encodeToString(bytes);
    }

    private String getEncoded(String num, String pass) {
        try {
            Connection connect = Jsoup.connect(URLENCODE)
                    .header("Accept",
                            "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                    .userAgent("Mozilla").method(Connection.Method.POST).timeout(3000);
            for (Map.Entry<String, String> entry : cookie.entrySet()) {
                connect.cookie(entry.getKey(), entry.getValue());
            }
            Connection.Response response = connect.execute();
            String dataStr = response.parse().text();
            // 把JS中的加密算法用Java写一遍：
            String scode = dataStr.split("#")[0];
            String sxh = dataStr.split("#")[1];
            String code = num + "%%%" + pass;
            StringBuilder encoded = new StringBuilder();
            for (int i = 0; i < code.length(); i++) {
                if (i < 20) {
                    encoded.append(code, i, i + 1).append(scode, 0, Integer.parseInt(sxh.substring(i, i + 1)));
                    scode = scode.substring(Integer.parseInt(sxh.substring(i, i + 1)), scode.length());
                } else {
                    encoded.append(code.substring(i));
                    i = code.length();
                }
            }
            return encoded.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

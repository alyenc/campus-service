package com.alyenc.campus.score;

import com.alyenc.campus.score.model.CourseResponse;
import com.alyenc.campus.score.model.ScoreResponse;
import com.alyenc.campus.score.model.VerifyCodeResponse;

public interface ScoreService {

    VerifyCodeResponse getVerifyCode();

    ScoreResponse getScore(String num, String pass, String code, String cookie);

    CourseResponse getCourses(String num, String pass, String code, String cookie, String condition);

    CourseResponse reloadCourses(String cookie, String condition);
}

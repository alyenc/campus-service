package com.alyenc.campus.score.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Getter
@Setter
@ToString
public class VerifyCodeResponse {

    private String verifyCode;

    private Map<String, String> cookie;
}

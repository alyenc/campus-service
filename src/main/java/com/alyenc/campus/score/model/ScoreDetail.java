package com.alyenc.campus.score.model;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
public class ScoreDetail {

    private String term;

    private String number;

    private String name;

    private String score;

    private String credit;

    private String point;

    private String attr;
}

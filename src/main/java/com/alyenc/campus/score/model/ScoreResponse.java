package com.alyenc.campus.score.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ScoreResponse {

    private List<ScoreDetail> scores;
}

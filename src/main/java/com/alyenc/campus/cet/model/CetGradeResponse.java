package com.alyenc.campus.cet.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class CetGradeResponse {

    private String name;

    private String school;

    private String no;

    private String grade;

    private String readGrade;

    private String listenGrade;

    private String writeGrade;
}

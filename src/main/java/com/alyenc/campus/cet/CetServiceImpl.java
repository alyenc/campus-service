package com.alyenc.campus.cet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alyenc.campus.cet.model.CetGradeResponse;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.alyenc.campus.show.ShowApiRequest;
import com.gargoylesoftware.htmlunit.html.HtmlSpan;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
public class CetServiceImpl implements CetService {

    private final static String MAIN_URL = "http://cet.neea.edu.cn/cet";

    @Override
    public CetGradeResponse getCet(String no, String name) {
        return getMainPage(no, name);
    }

    private CetGradeResponse getMainPage(String no, String name) {
        try {
            WebClient webClient = new WebClient(BrowserVersion.CHROME);
            webClient.addRequestHeader("User-Agent", "Mozilla/5.0 (iPad; CPU OS 7_0_2 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A501 Safari/9537.53");
            HtmlPage page = webClient.getPage(MAIN_URL);
            //等待页面加载
            Thread.sleep(1000);

            //获取输入帐号的控件
            HtmlInput noInput = (HtmlInput) page.getElementById("zkzh");
            noInput.setValueAttribute(no);

            //获取输入密码的控件
            HtmlInput nameInput = (HtmlInput) page.getElementById("name");
            nameInput.setValueAttribute(name);

            //获取输入密码的控件
            HtmlInput verifyInput = (HtmlInput) page.getElementById("verify");
            verifyInput.focus();


            HtmlImage verifyImage = (HtmlImage) page.getElementById("img_verifys");
            String imageUrl = verifyImage.getSrcAttribute();
            String base64Image = getImageStr(imageUrl);
            String verify = showVerify(base64Image);
            if(null != verify) {
                verifyInput.setValueAttribute(verify);
                HtmlInput submitButton = (HtmlInput) page.getElementById("submitButton");
                HtmlPage resultPage = submitButton.click();

                HtmlSpan nameSpan = (HtmlSpan) resultPage.getElementById("n");
                HtmlSpan schoolSpan = (HtmlSpan) resultPage.getElementById("x");
                HtmlSpan noSpan = (HtmlSpan) resultPage.getElementById("z");
                HtmlSpan gradeSpan = (HtmlSpan) resultPage.getElementById("s");
                HtmlSpan listenSpan = (HtmlSpan) resultPage.getElementById("l");
                HtmlSpan readSpan = (HtmlSpan) resultPage.getElementById("r");
                HtmlSpan writeSpan = (HtmlSpan) resultPage.getElementById("w");


                CetGradeResponse response = new CetGradeResponse();
                response.setName(nameSpan.asText());
                response.setNo(noSpan.asText());
                response.setGrade(gradeSpan.asText());
                response.setListenGrade(listenSpan.asText());
                response.setReadGrade(readSpan.asText());
                response.setSchool(schoolSpan.asText());
                response.setWriteGrade(writeSpan.asText());

                return response;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String showVerify(String base64Image) {
        String res=new ShowApiRequest("http://route.showapi.com/184-5","64732","cbfdfb426847438c935952547d4e12a7")
                .addTextPara("img_base64",base64Image)
                .addTextPara("typeId","34")
                .addTextPara("convert_to_jpg","0")
                .addTextPara("needMorePrecise","0")
                .post();

        JSONObject jsonObject = JSON.parseObject(res);
        Integer code = jsonObject.getInteger("showapi_res_code");
        if(code == 0) {
            JSONObject body = jsonObject.getJSONObject("showapi_res_body");
            return body.getString("Result");
        }

        return null;
    }

    private String getImageStr(String imgFile) {
        ByteArrayOutputStream outPut = new ByteArrayOutputStream();
        byte[] data = new byte[1024];
        try {
            // 创建URL
            URL url = new URL(imgFile);
            // 创建链接
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(10 * 1000);

            if(conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "fail";//连接失败/链接失效/图片不存在
            }
            InputStream inStream = conn.getInputStream();
            int len = -1;
            while ((len = inStream.read(data)) != -1) {
                outPut.write(data, 0, len);
            }
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(outPut.toByteArray());
    }
}

package com.alyenc.campus.cet;

import com.alyenc.campus.cet.model.CetGradeResponse;

public interface CetService {

    CetGradeResponse getCet(String no, String name);
}

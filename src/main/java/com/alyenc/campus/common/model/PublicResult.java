package com.alyenc.campus.common.model;

import com.alyenc.campus.common.enums.ResultEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PublicResult<T> {

    private int code;

    private String msg;

    private boolean success;

    private T value;

    public static <T> PublicResult<T> success(T value){
        PublicResult<T> result = new PublicResult<>();
        result.setSuccess(true);
        result.setValue(value);
        return result;
    }

    public static <T> PublicResult<T> fail(int code, String msg){
        PublicResult<T> result = new PublicResult<>();
        result.setSuccess(false);
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public static <T> PublicResult<T> fail(ResultEnum resultEnum){
        PublicResult<T> result = new PublicResult<>();
        result.setSuccess(false);
        result.setCode(resultEnum.getCode());
        result.setMsg(resultEnum.getMsg());
        return result;
    }
}

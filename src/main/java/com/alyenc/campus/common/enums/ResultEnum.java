package com.alyenc.campus.common.enums;

import lombok.Getter;

@Getter
public enum ResultEnum {

    PARAMS_MISSING(1000, "参数缺失");

    private int code;

    private String msg;

    ResultEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

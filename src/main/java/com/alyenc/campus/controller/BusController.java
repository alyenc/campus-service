package com.alyenc.campus.controller;

import com.alyenc.campus.bus.BusService;
import com.alyenc.campus.bus.response.BusListResponse;
import com.alyenc.campus.bus.response.BusStationResponse;
import com.alyenc.campus.bus.response.RealTimeResponse;
import com.alyenc.campus.common.enums.ResultEnum;
import com.alyenc.campus.common.model.PublicResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class BusController {

    @Autowired
    private BusService busService;

    @RequestMapping("/getBusStation.json")
    public PublicResult<BusStationResponse> getBusStation(@RequestBody Map<String, String> param) {
        String busline = param.get("busline");
        if(null == busline) {
            return PublicResult.fail(ResultEnum.PARAMS_MISSING);
        }
        BusStationResponse response = busService.getBusStation(busline);
        System.out.println(response);
        return PublicResult.success(response);
    }

    @RequestMapping("/getBusList.json")
    public PublicResult<BusListResponse> getBusList(@RequestBody Map<String, String> param) {
        String busline = param.get("busline");
        if(null == busline) {
            return PublicResult.fail(ResultEnum.PARAMS_MISSING);
        }
        BusListResponse response = busService.getBusList(busline);
        return PublicResult.success(response);
    }

    @RequestMapping("/getRealTimeBus.json")
    public PublicResult<RealTimeResponse> getRealTimeBus(@RequestBody Map<String, String> param) {
        String busline = param.get("busline");
        if(null == busline) {
            return PublicResult.fail(ResultEnum.PARAMS_MISSING);
        }
        RealTimeResponse response = busService.getRealTimeBus(busline);
        return PublicResult.success(response);
    }
}

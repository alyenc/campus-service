package com.alyenc.campus.controller;

import com.alyenc.campus.common.enums.ResultEnum;
import com.alyenc.campus.common.model.PublicResult;
import com.alyenc.campus.news.NewsService;
import com.alyenc.campus.news.model.NewsListResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class NewsController {

    @Autowired
    private NewsService newsService;

    @RequestMapping("/getNewsList.json")
    public PublicResult getNewsList() {
        NewsListResponse newsListResponse = newsService.getNewsList();
        return PublicResult.success(newsListResponse);
    }

    @RequestMapping("/getNewsDetail.json")
    public PublicResult getNewsDetail(@RequestBody Map<String, String> param) {
        String url = param.get("url");
        if(null == url) {
            return PublicResult.fail(ResultEnum.PARAMS_MISSING);
        }
        List<Map<String, Object>> newsDetail = newsService.getNewsDetail(url);
        return PublicResult.success(newsDetail);
    }
}

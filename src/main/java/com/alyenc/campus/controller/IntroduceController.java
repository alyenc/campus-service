package com.alyenc.campus.controller;

import com.alyenc.campus.common.model.PublicResult;
import com.alyenc.campus.news.model.NewsDetail;
import com.alyenc.campus.show.util.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class IntroduceController {

    private static final String url = "http://www.qlu.edu.cn/5215/list.htm";

    @RequestMapping("/getIntroduce.json")
    public PublicResult<List<Map<String, Object>>> getIntroduce() {
        List<Map<String, Object>> resp = getIntroduceImpl();
        return PublicResult.success(resp);
    }

    private List<Map<String, Object>> getIntroduceImpl() {
        List<Map<String, Object>> list = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(url).get();
            Element element = doc.getElementById("wp_content_w6_0");
            Elements ps = element.select("p");
            for (Element e : ps) {
                Map<String, Object> map = new HashMap<>();
                Element span = e.getElementsByTag("span").first();
                Element image = e.getElementsByTag("img").first();
                if(null != span) {
                    if(!StringUtils.isEmpty(span.text())) {
                        map.put("type", 0);
                        map.put("content", span.text());
                    }
                } else if(null != image) {
                    String src = image.attr("src");
                    String fullSrc = String.format("https://campus.alyenc.com%s", src);

                    map.put("type", 2);
                    map.put("content", fullSrc);
                } else {
                    if(!StringUtils.isEmpty(e.text())) {
                        map.put("type", 1);
                        map.put("content", e.text());
                    }
                }
                list.add(map);
            }
            System.out.println(doc.html());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }
}

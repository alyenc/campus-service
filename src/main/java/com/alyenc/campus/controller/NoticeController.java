package com.alyenc.campus.controller;

import com.alyenc.campus.common.enums.ResultEnum;
import com.alyenc.campus.common.model.PublicResult;
import com.alyenc.campus.news.model.NewsListResponse;
import com.alyenc.campus.notice.NoticeService;
import com.alyenc.campus.notice.model.NoticeListResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class NoticeController {

    @Autowired
    private NoticeService noticeService;

    @RequestMapping("/getNoticeList.json")
    public PublicResult getNoticeList() {
        NoticeListResponse response = noticeService.getNoticeList();
        return PublicResult.success(response);
    }

    @RequestMapping("/getNoticeDetail.json")
    public PublicResult getNoticeDetail(@RequestBody Map<String, String> param) {
        String url = param.get("url");
        if(null == url) {
            return PublicResult.fail(ResultEnum.PARAMS_MISSING);
        }
        List<Map<String, Object>> resp = noticeService.getNoticeDetail(url);
        return PublicResult.success(resp);
    }
}

package com.alyenc.campus.controller;

import com.alyenc.campus.common.enums.ResultEnum;
import com.alyenc.campus.common.model.PublicResult;
import com.alyenc.campus.express.ExpressService;
import com.alyenc.campus.express.response.ExpressCodeResponse;
import com.alyenc.campus.express.response.ExpressResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ExpressController {

    @Autowired
    private ExpressService expressService;

    @RequestMapping("/getExpressCode.json")
    public PublicResult<ExpressCodeResponse> getExpressCode() {
        ExpressCodeResponse response = expressService.getExpressCode();
        return PublicResult.success(response);
    }

    @RequestMapping("/getExpress.json")
    public PublicResult<ExpressResponse> getExpress(@RequestBody Map<String, String> param) {
        String com = param.get("com");
        String num = param.get("num");
        if(null == com || null == num) {
            return PublicResult.fail(ResultEnum.PARAMS_MISSING);
        }
        ExpressResponse response = expressService.getExpress(com, num);
        return PublicResult.success(response);
    }
}

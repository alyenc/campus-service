package com.alyenc.campus.controller;

import com.alyenc.campus.cet.CetService;
import com.alyenc.campus.cet.model.CetGradeResponse;
import com.alyenc.campus.common.model.PublicResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class CetController {

    @Autowired
    private CetService cetService;

    @RequestMapping("/getCetGrade.json")
    public PublicResult getCetGrade(@RequestBody Map<String, String> params) {
        String no = params.get("no");
        String name = params.get("name");
        CetGradeResponse response = cetService.getCet(no, name);
        return PublicResult.success(response);
    }
}

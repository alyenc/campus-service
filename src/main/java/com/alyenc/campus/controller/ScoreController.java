package com.alyenc.campus.controller;

import com.alyenc.campus.common.enums.ResultEnum;
import com.alyenc.campus.common.model.PublicResult;
import com.alyenc.campus.score.ScoreService;
import com.alyenc.campus.score.model.CourseResponse;
import com.alyenc.campus.score.model.ScoreResponse;
import com.alyenc.campus.score.model.VerifyCodeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ScoreController {

    @Autowired
    private ScoreService scoreService;

    @RequestMapping("getVerifyCode.json")
    public PublicResult getVerifyCode() {
        VerifyCodeResponse response = scoreService.getVerifyCode();
        return PublicResult.success(response);
    }

    @RequestMapping("/getScore.json")
    public PublicResult getScore(@RequestBody Map<String, String> param) {
        String num = param.get("num");
        String pass = param.get("pass");
        String code = param.get("verify");
        String cookie = param.get("cookie");
        if(null == num || null == pass || null == code || null == cookie) {
            return PublicResult.fail(ResultEnum.PARAMS_MISSING);
        }
        ScoreResponse response = scoreService.getScore(num, pass, code, cookie);

        return PublicResult.success(response);
    }

    @RequestMapping("/getCourse.json")
    public PublicResult getCourse(@RequestBody Map<String, String> param) {
        String num = param.get("num");
        String pass = param.get("pass");
        String code = param.get("verify");
        String cookie = param.get("cookie");
        String condition = param.get("condition");
        if(null == num || null == pass || null == code || null == cookie || null == condition) {
            return PublicResult.fail(ResultEnum.PARAMS_MISSING);
        }
        CourseResponse response = scoreService.getCourses(num, pass, code, cookie, condition);

        return PublicResult.success(response);
    }

    @RequestMapping("reloadCourses.json")
    public PublicResult reloadCourses(@RequestBody Map<String, String> param) {
        String cookie = param.get("cookie");
        String condition = param.get("condition");
        if(null == cookie || null == condition) {
            return PublicResult.fail(ResultEnum.PARAMS_MISSING);
        }
        CourseResponse response = scoreService.reloadCourses(cookie, condition);
        return PublicResult.success(response);
    }
}

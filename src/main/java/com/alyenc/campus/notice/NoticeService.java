package com.alyenc.campus.notice;

import com.alyenc.campus.notice.model.NoticeDetail;
import com.alyenc.campus.notice.model.NoticeListResponse;

import java.util.List;
import java.util.Map;

public interface NoticeService {

    NoticeListResponse getNoticeList();

    List<Map<String, Object>> getNoticeDetail(String url);
}

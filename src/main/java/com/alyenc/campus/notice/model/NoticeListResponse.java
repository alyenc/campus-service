package com.alyenc.campus.notice.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class NoticeListResponse {

    private List<NoticeDetail> noticeList;

}

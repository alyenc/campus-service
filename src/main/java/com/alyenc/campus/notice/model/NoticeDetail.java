package com.alyenc.campus.notice.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class NoticeDetail {

    private String title;
    private String desc;
    private String publishTime;
    private String from;
    private String editor;
    private String author;
    private String url;
}

package com.alyenc.campus.notice;

import com.alyenc.campus.news.model.NewsDetail;
import com.alyenc.campus.notice.model.NoticeDetail;
import com.alyenc.campus.notice.model.NoticeListResponse;
import com.alyenc.campus.show.util.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NoticeServiceImpl implements NoticeService {

    private final static String url = "http://www.qlu.edu.cn/39/list.htm";

    private final static String base_url = "http://www.qlu.edu.cn";

    @Override
    public NoticeListResponse getNoticeList() {
        NoticeListResponse response = new NoticeListResponse();
        List<NoticeDetail> noticeDetails = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(url).get();
            Element noticeListElement = doc.getElementById("wp_news_w6");
            Elements ElementsUl = noticeListElement.getElementsByTag("ul");
            Element elementLi = ElementsUl.get(0);
            Elements newsEl = elementLi.getElementsByTag("li");
            for (Element element : newsEl) {
                Element el = element.select("a").first();
                String url = el != null ? el.attr("href") : "";
                String noticeUrl = "";
                if(url.contains("http") || url.contains("https")) {
                    continue;
                } else {
                    noticeUrl = base_url + url;
                }
                NoticeDetail noticeDetail = getNoticeDetailList(noticeUrl);
                noticeDetails.add(noticeDetail);
            }
            response.setNoticeList(noticeDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private NoticeDetail getNoticeDetailList(String url) {
        NoticeDetail noticeDetail = new NoticeDetail();
        noticeDetail.setUrl(url);
        try {
            Document doc = Jsoup.connect(url).get();

            Elements elements = doc.getElementsByTag("title");
            Element element = elements.get(0);
            noticeDetail.setTitle(element.text());
            noticeDetail.setDesc(doc.select("meta[name=description]").get(0).attr("content"));

            Elements publishElements = doc.getElementsByClass("arti_update");
            Element publishElement = publishElements.get(0);
            noticeDetail.setPublishTime(publishElement.text());

            Elements authorElements = doc.getElementsByClass("arti_author");
            Element authorElement = authorElements.get(0);
            noticeDetail.setAuthor(authorElement.text());

            Elements infoElements = doc.getElementsByClass("arti_info");
            Element infoElement = infoElements.get(0);
            noticeDetail.setFrom(infoElement.text());

            Elements editoElements = doc.getElementsByClass("arti_edito");
            Element editoElement = editoElements.get(0);
            noticeDetail.setEditor(editoElement.text());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return noticeDetail;
    }

    @Override
    public List<Map<String, Object>> getNoticeDetail(String url) {
        List<Map<String, Object>> contentList = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(url).get();

            doc.select("#header").remove();
            doc.select("#nav").remove();
            doc.select("#wp-navi-aside").remove();
            doc.select("#container-1").remove();
            doc.select(".col_news_head").remove();
            doc.select(".foot-right").remove();
            doc.select("meta").remove();
            Elements contents = doc.select(".wp_articlecontent");
            Element content = contents.get(0);
            Elements ps = content.getElementsByTag("p");

            String titleContent = doc.select("title").first().text();
            Element authorElement = doc.select(".arti_metas").first();
            Elements authorSpans = authorElement.select("span");
            StringBuilder authorContent = new StringBuilder();
            for (Element e : authorSpans) {
                authorContent.append(e.text());
                authorContent.append("&nbsp;&nbsp;");
            }
            Map<String, Object> title = new HashMap<>();
            Map<String, Object> author = new HashMap<>();
            title.put("type", "title");
            title.put("content", titleContent);
            author.put("type", "author");
            author.put("content", authorContent);
            contentList.add(title);
            contentList.add(author);
            for (Element e : ps) {
                Map<String, Object> map = new HashMap<>();
                Elements imgs = e.getElementsByTag("img");
                if(null == imgs.first()) {
                    if(StringUtils.isEmpty(e.text())) {
                        continue;
                    }
                    map.put("type", 1);
                    map.put("content", e.text());
                } else {
                    Element img = imgs.first();
                    String src = img.attr("src");
                    String fullSrc = String.format("https://campus.alyenc.com%s", src);
                    map.put("type", 2);
                    map.put("content", fullSrc);
                }
                contentList.add(map);
            }
            return contentList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<Map<String, Object>>();
    }
}

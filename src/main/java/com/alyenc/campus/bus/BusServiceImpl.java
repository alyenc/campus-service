package com.alyenc.campus.bus;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alyenc.campus.bus.model.BusStationDetail;
import com.alyenc.campus.bus.model.RealTimeBusDetail;
import com.alyenc.campus.bus.response.BusLineItem;
import com.alyenc.campus.bus.response.BusListResponse;
import com.alyenc.campus.bus.response.BusStationResponse;
import com.alyenc.campus.bus.response.RealTimeResponse;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class BusServiceImpl implements BusService {

    private final static String searchUrl = "http://60.216.101.229:80/server-ue2/rest/buslines/simple/370100/%s/0/20";

    private final static String realTimeBusUrl = "http://60.216.101.229:80/server-ue2/rest/buses/busline/370100/%s";

    private final static String stationUrl = "http://60.216.101.229:80/server-ue2/rest/buslines/370100/%s";

    private final static int OK_CODE = 0;

    @Override
    public BusStationResponse getBusStation(String busline) {
        String realUrl = String.format(stationUrl, busline);
        BusStationResponse response = new BusStationResponse();
        List<BusStationDetail> list = new ArrayList<>();
        try {
            String resp = okhttpGet(realUrl);
            JSONObject jsonObject = JSONObject.parseObject(resp);

            System.out.println(jsonObject.toJSONString());
            JSONObject statusObject = jsonObject.getJSONObject("status");
            Integer code = statusObject.getInteger("code");

            if(code == OK_CODE) {
                JSONObject resultObject = jsonObject.getJSONObject("result");
                JSONArray innerArray = resultObject.getJSONArray("stations");
                innerArray.forEach(item -> {
                    String itemStr = JSONObject.toJSONString(item);
                    JSONObject itemObject = JSON.parseObject(itemStr);
                    BusStationDetail busLine = BusStationDetail.builder()
                            .id(itemObject.getString("id"))
                            .area(itemObject.getString("area"))
                            .stationName(itemObject.getString("stationName"))
                            .lng(itemObject.getDouble("lng"))
                            .lat(itemObject.getDouble("lat"))
                            .buslines(itemObject.getString("buslines"))
                            .state(itemObject.getInteger("state"))
                            .updateTime(itemObject.getString("updateTime"))
                            .distance(itemObject.getDouble("distance")).build();

                    list.add(busLine);
                });

                response.setTicketPrice(resultObject.getString("ticketPrice"));
                response.setOperationTime(resultObject.getString("operationTime"));
                response.setStartStationName(resultObject.getString("startStationName"));
                response.setEndStationName(resultObject.getString("endStationName"));
                response.setLineName(resultObject.getString("lineName"));
                response.setBusStationDetailList(list);
            }
            return response;
        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public BusListResponse getBusList(String busline) {
        BusListResponse response = new BusListResponse();
        List<BusLineItem> itemList = new ArrayList<>();

        String realUrl = String.format(searchUrl, busline);
        String resp;
        try {
            resp = okhttpGet(realUrl);
            JSONObject jsonObject = JSONObject.parseObject(resp);

            JSONObject statusObject = jsonObject.getJSONObject("status");
            Integer code = statusObject.getInteger("code");
            if(code == OK_CODE) {
                JSONObject resultObject = jsonObject.getJSONObject("result");
                JSONArray innerArray = resultObject.getJSONArray("result");
                innerArray.forEach(item -> {
                    String itemStr = JSONObject.toJSONString(item);
                    JSONObject itemObject = JSON.parseObject(itemStr);
                    BusLineItem busLine = BusLineItem.builder()
                            .id(itemObject.getString("id"))
                            .lineName(itemObject.getString("lineName"))
                            .direction(itemObject.getString("startStationName") + " - " + itemObject.getString("endStationName"))
                            .startStationName(itemObject.getString("startStationName"))
                            .endStationName(itemObject.getString("endStationName")).build();

                    itemList.add(busLine);
                });
                response.setBusLines(itemList);
            }
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public RealTimeResponse getRealTimeBus(String busline) {
        RealTimeResponse response = new RealTimeResponse();
        List<RealTimeBusDetail> realTimeList = new ArrayList<>();

        String realUrl = String.format(realTimeBusUrl, busline);
        String resp;
        try {
            resp = okhttpGet(realUrl);
            JSONObject jsonObject = JSONObject.parseObject(resp);
            System.out.println(jsonObject);
            JSONObject statusObject = jsonObject.getJSONObject("status");
            Integer code = statusObject.getInteger("code");
            if(code == OK_CODE) {
                JSONArray resultArray = jsonObject.getJSONArray("result");
                resultArray.forEach(item -> {
                    String itemStr = JSONObject.toJSONString(item);
                    JSONObject itemObject = JSON.parseObject(itemStr);

                    RealTimeBusDetail realTimeBusDetail = RealTimeBusDetail.builder()
                            .busId(itemObject.getString("busId"))
                            .buslineId(itemObject.getString("buslineId"))
                            .orgName(itemObject.getString("orgName"))
                            .coordinate(itemObject.getString("coordinate"))
                            .lng(itemObject.getString("lng"))
                            .isArrvLft(itemObject.getString("isArrvLft"))
                            .nextStationMinutes(itemObject.getString("nextStationMinutes"))
                            .actTime(itemObject.getString("actTime"))
                            .stationSeqNum(itemObject.getString("stationSeqNum"))
                            .averageVelocity(itemObject.getString("averageVelocity"))
                            .velocity(itemObject.getString("velocity"))
                            .nextStationDistance(itemObject.getString("nextStationDistance"))
                            .busLineName(itemObject.getString("busLineName"))
                            .cardId(itemObject.getString("cardId"))
                            .lat(itemObject.getString("lat"))
                            .nextStation(itemObject.getString("nextStation"))
                            .status(itemObject.getString("status")).build();

                    realTimeList.add(realTimeBusDetail);
                });
                response.setRealTimeBusDetailList(realTimeList);
            }
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String okhttpGet(String url) throws IOException {
        Request request = new Request.Builder()
                .addHeader("version", "android-insigma.waybook.jinan-2363")
                .url(url)
                .get().build();

        OkHttpClient okHttpClient = new OkHttpClient();
        Response response = okHttpClient.newCall(request).execute();
        if (response.isSuccessful()) {
            return response.body().string();
        } else {
            throw new IOException("Unexpected code " + response);
        }

    }
}

package com.alyenc.campus.bus.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class BusLineItem {

    /**
     * 线路ID
     */
    private String id;

    /**
     * 线路名称
     */
    private String lineName;

    /**
     * 起始站
     */
    private String startStationName;

    /**
     * 终点站
     */
    private String endStationName;

    private String direction;
}

package com.alyenc.campus.bus.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class BusListResponse {

    private List<BusLineItem> busLines;
}

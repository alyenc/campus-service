package com.alyenc.campus.bus.response;

import com.alyenc.campus.bus.model.BusStationDetail;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
public class BusStationResponse {

    List<BusStationDetail> busStationDetailList;

    String startStationName;

    String endStationName;

    String operationTime;

    String ticketPrice;

    String lineName;
}

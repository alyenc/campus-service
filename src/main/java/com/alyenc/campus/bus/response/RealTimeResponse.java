package com.alyenc.campus.bus.response;

import com.alyenc.campus.bus.model.RealTimeBusDetail;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
public class RealTimeResponse {

    private List<RealTimeBusDetail> realTimeBusDetailList;

}

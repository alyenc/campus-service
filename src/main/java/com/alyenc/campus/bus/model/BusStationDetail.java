package com.alyenc.campus.bus.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
public class BusStationDetail {

    private String id;
    private String area;
    private String stationName;
    private Double lng;
    private Double lat;
    private String buslines;
    private Integer state;
    private String updateTime;
    private Double distance;
}

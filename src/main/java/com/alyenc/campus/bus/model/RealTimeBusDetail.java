package com.alyenc.campus.bus.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class RealTimeBusDetail {

    private String busId;

    private String buslineId;

    private String orgName;

    private String coordinate;

    private String lng;

    private String isArrvLft;

    private String nextStationMinutes;

    private String actTime;

    private String stationSeqNum;

    private String averageVelocity;

    private String velocity;

    private String nextStationDistance;

    private String busLineName;

    private String cardId;

    private String lat;

    private String nextStation;

    private String status;
}

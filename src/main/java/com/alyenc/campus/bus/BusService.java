package com.alyenc.campus.bus;

import com.alyenc.campus.bus.response.BusListResponse;
import com.alyenc.campus.bus.response.BusStationResponse;
import com.alyenc.campus.bus.response.RealTimeResponse;

public interface BusService {

    BusStationResponse getBusStation(String busline);

    BusListResponse getBusList(String busline);

    RealTimeResponse getRealTimeBus(String busline);
}
